#ifndef CHAT_H
#define CHAT_H

void chatInit();

int addUser(char nick[20], long pid);

int removeUser(int index);

int getUserIndexName(char name[20]);

int getUserIndexPid(long pid);

long getUserPid(int index);

void getUserName(int index, char name[20]);

void setUserName(int index, char newName[20]);

int getUserGroup(int index);

int joinGroup(int user, int group);

int leaveGroup(int user);

int getGroupIndex(char name[20]);

void getGroupPid(int index, long groupUsers[20]);

void getGroupList( char tab[1000]);

void getUsersList(char tab[1000]);

#endif