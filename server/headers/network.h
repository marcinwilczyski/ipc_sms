#ifndef NETWORK_H
#define NETWORK_H

typedef struct msgbuf msgbuf;

struct msgbuf 
{
	long type;
	int cmd;
	int pid;
	char nick[20];
	int status;
	char tab[1000];
	char date[20];
};

/* creates, or opens queue to send messages, returns id to queue */
void establishConnection(int key);

int sendRegisterConfirmation( long pid, int status);

int sendUsersList( long pid, char nicks[1000]);

int sendGrupsList( long pid, char groups[1000]);

int sendNameChangedConfirmation( long pid, int status);

int sendSignToGroupConfirmation(long pid, int status);

int sendUnsignFromGroupConfirmation(long pid, int status);

int sendPrivateMessageConfirmation( long pid, int status);

int sendPrivateMessage( long pid, char date[20], char tab[1000], char nick[20]);

int sendGroupMessageConfirmation( long pid, int status);

int sendGroupMessage(long pid, char date[20], char tab[1000], char nick[20]);

int sendLogOutConfirmation( long pid, int status);

msgbuf getRequest();

#endif