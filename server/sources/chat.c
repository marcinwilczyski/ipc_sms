#include "../headers/chat.h"

#include<string.h>

typedef struct user user;

struct user
{
	long pid;
	char nick[20];
	int group;
};

char groupNames[3][20];
user users[20];

void groupNamesInit()
{
	strncpy(groupNames[0],"dzicy",20);
	strncpy(groupNames[1],"helpers",20);
	strncpy(groupNames[2],"nans",20);
}

void usersInit()
{
	int i;
	for(i=0;i<20;i++)
	{
		users[i].pid = -1;
		users[i].group = -1;
	}
}

void chatInit()
{
	groupNamesInit();
	usersInit();
}

int addUser(char nick[20], long pid)
{
	int status = -1;
	int i;
	for(i=0;i<20;i++)
	{
		if(users[i].pid == -1)
		{
			status = i;
			users[i].pid = pid;
			strncpy(users[i].nick,nick,20);
			break;
		}
	}
	return status;
}

int removeUser(int index)
{
	users[index].pid = -1;
	strncpy(users[index].nick,"",1);
	return 0;
}

int getGroupIndex(char name[20])
{
	int index = -1;
	int i;
	for(i=0;i<3;i++)
	{
		if(strncmp(groupNames[i], name, 20) == 0)
		{
			index = i;
		}
	}
	return index;
}

int getUserIndexName(char name[20])
{
	int index = -1;
	int i;
	for(i=0;i<20;i++)
	{
		if(strncmp(users[i].nick, name, 20)==0)
		{
			index = i;
			break;
		}
	}
	return index;
}

int getUserIndexPid(long pid)
{
	int index = -1;
	int i;
	for(i=0;i<20;i++)
	{
		if(users[i].pid == pid)
		{
			index = i;
		}
	}
	return index;
}

long getUserPid(int index)
{
	return users[index].pid;
}

void getUserName(int index, char name[20])
{
	strncpy(name, users[index].nick,20);
}

void setUserName(int index, char newName[20])
{
	strncpy(users[index].nick, newName, 20);
}

int getUserGroup(int index)
{
	return users[index].group;
}

void getGroupPid(int index, long groupUsers[20])
{
	int i;
	for(i=0;i<20;i++)
	{
		groupUsers[i] = -1;
	}
	for(i=0;i<20;i++)
	{
		if(users[i].group==index)
		{
			groupUsers[i] = users[i].pid;
		}
	}
}

int joinGroup(int user, int group)
{
	int status = -1;
	if(users[user].group == -1)
	{
		users[user].group = group;
		status = 0;
	}
	return status;
}

int leaveGroup(int user)
{
	int status = -1;
	if(users[user].group!=-1)
	{
		users[user].group = -1;
		status = 0;
	}
	return status;
}

void getGroupList(char tab[1000])
{
	int i;
	char separator = ';';
	for(i=0;i<3;i++)
	{
		if(i!=0) strncat(tab, &separator,1);
		strncat(tab,groupNames[i],20);
	}
}

void getUsersList(char tab[1000])
{
	int i;
	char separator = ';';
	int number = 0;
	for(i=0;i<20;i++)
	{
		if(users[i].pid != -1)
		{
			if(number!=0)
			{
				strncat(tab, &separator, 1);
			}
			strncat(tab,users[i].nick,20);
			number++;
		}
	}
}

