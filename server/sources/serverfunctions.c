#include "../headers/serverfunctions.h"
#include "../headers/chat.h"
#include "../headers/network.h"

#include<stdio.h>


int registerUser(msgbuf message)
{
	int status = 1;
	if(getUserIndexName(message.nick) != -1)
	{
		status = 2;
	}
	if(getUserIndexPid(message.pid) != -1)
	{
		status = 3;		
	}
	if(status == 1)
	{
		if(addUser(message.nick, message.pid) == -1)
		{
			status = 9;
		}
	}
	return sendRegisterConfirmation(message.pid, status);
}

int userList(msgbuf message)
{
	char tab[1000]="";
	getUsersList(tab);
	return sendUsersList(message.pid,tab);
}

int groupList(msgbuf message)
{
	char tab[1000]="";
	getGroupList(tab);
	return sendGrupsList(message.pid, tab);
	
}

int renameUser(msgbuf message)
{
	int status = 1;
	int index = getUserIndexPid(message.pid);
	if(index == -1)
	{
		status = 8;
	}
	if(status==1 && getUserIndexName(message.nick) !=-1)
	{
		status = 2;
	}
	if(status==1)
	{	
		setUserName(index, message.nick);
	}
	return sendNameChangedConfirmation(message.pid, status);
}

int signInToGroup(msgbuf message)
{
	int status = 1;
	int group = getGroupIndex(message.nick);
	int user = getUserIndexPid(message.pid);
	if(user == -1)
	{
		status = 8;
	}
	else if(group == -1)
	{
		status = 5;
	}
	else if(joinGroup(user,group) == -1)
	{
		status = 4;
	}
	return sendSignToGroupConfirmation(message.pid,status);
	
}

int signOutFromGroup(msgbuf message)
{
	int status = 1;
	int user = getUserIndexPid(message.pid);
	if(user == -1)
	{
		status = 8;
	}
	else if(leaveGroup(user) == -1)
	{
		status = 6;
	}
	return sendUnsignFromGroupConfirmation(message.pid,status);
}

int sendPrivate(msgbuf message)
{
	int status = 1;
	int user = getUserIndexPid(message.pid);
	int receiver = getUserIndexName(message.nick);
	if(user == -1)
	{
		status = 8;
	}
	else if(receiver == -1)
	{
		status = 7;
	}
	if(status == 1)
	{
		char nick[20] = "";
		getUserName(user, nick);
		return sendPrivateMessageConfirmation(message.pid,status)+
		sendPrivateMessage(getUserPid(receiver), message.date,message.tab,nick);
	}
	else
	{
		return sendPrivateMessageConfirmation(message.pid,status);
	}
}

int sendGroup(msgbuf message)
{
	int status = 1;
	int user = getUserIndexPid(message.pid);
	int group;
	if(user !=-1 ) group=getUserGroup(user); 
	if(user == -1)
	{
		status = 8;
	}
	else if(group == -1)
	{
		status = 6;
	}
	if(status == 1)
	{
		char nick[20] = "";
		getUserName(user, nick);
		long groupUsers[20];
		getGroupPid(group, groupUsers);
		int i;
		for(i = 0;i<20;i++)
		{
			if(groupUsers[i]!=-1 && groupUsers[i]!= message.pid)
				sendGroupMessage(groupUsers[i], message.date,message.tab,nick);
		}	
	}
	return sendGroupMessageConfirmation(message.pid,status);
}

int logOut(msgbuf message)
{
	int status = 1;
	int user = getUserIndexPid(message.pid);
	if(user == -1)
	{
		status = 8;
	}
	else
	{
		removeUser(user);
	}
	return sendLogOutConfirmation(message.pid,status);
}

void listen()
{
	msgbuf message = getRequest();
	if(message.cmd == 1)
	{
		registerUser(message);
	}
	else if(message.cmd == 2)
	{
		userList(message);
	}
	else if(message.cmd == 3)
	{
		groupList(message);
	}
	else if(message.cmd == 4)
	{
		renameUser(message);
	}
	else if(message.cmd == 5)
	{
		signInToGroup(message);
	}
	else if(message.cmd == 6)
	{
		signOutFromGroup(message);
	}
	else if(message.cmd == 8)
	{
		sendPrivate(message);
	}
	else if(message.cmd == 9)
	{
		sendGroup(message);
	}
	else if(message.cmd == 10)
	{
		logOut(message);
	}
}

void initServerFunctions(int key)
{
	establishConnection(key);
	chatInit();
}