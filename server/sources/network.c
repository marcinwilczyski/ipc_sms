#include "../headers/network.h"

#include<sys/types.h>
#include<sys/ipc.h>
#include<sys/msg.h>
#include<string.h>

int queueId;

void establishConnection(int key)
{
	queueId = msgget(key, IPC_CREAT | 0644);
}

int sendRegisterConfirmation( long pid, int status)
{
	msgbuf message;
	message.type = pid;
	message.cmd = 1;
	message.status = status;
	return msgsnd(queueId, &message,sizeof(message) - sizeof(long), IPC_NOWAIT);
}

int sendUsersList( long pid, char nicks[1000])
{
	msgbuf message;
	message.type = pid;
	message.cmd = 2;
	strncpy(message.tab, nicks,1000);
	return msgsnd(queueId, &message,sizeof(message) - sizeof(long), IPC_NOWAIT);
}

int sendGrupsList( long pid, char groups[1000])
{
	msgbuf message;
	message.type = pid;
	message.cmd = 3;
	strncpy(message.tab, groups, 1000);
	return msgsnd(queueId, &message,sizeof(message) - sizeof(long), IPC_NOWAIT);
}

int sendNameChangedConfirmation( long pid, int status)
{
	msgbuf message;
	message.type = pid;
	message.cmd = 4;
	message.status = status;
	return msgsnd(queueId, &message,sizeof(message) - sizeof(long), IPC_NOWAIT);
}

int sendSignToGroupConfirmation( long pid, int status)
{
	msgbuf message;
	message.type = pid;
	message.cmd = 5;
	message.status = status;
	return msgsnd(queueId, &message,sizeof(message) - sizeof(long), IPC_NOWAIT);
}

int sendUnsignFromGroupConfirmation( long pid, int status)
{
	msgbuf message;
	message.type = pid;
	message.cmd = 6;
	message.status = status;
	return msgsnd(queueId, &message,sizeof(message) - sizeof(long), IPC_NOWAIT);
}

int sendPrivateMessageConfirmation( long pid, int status)
{
	msgbuf message;
	message.type = pid;
	message.cmd = 8;
	message.status = status;
	message.tab[0]='\0';
	return msgsnd(queueId, &message,sizeof(message) - sizeof(long), IPC_NOWAIT);
}

int sendPrivateMessage( long pid, char date[20], char tab[1000], char nick[20])
{
	msgbuf message;
	message.type = pid;
	message.cmd = 8;
	strncpy(message.tab,tab,1000);
	strncpy(message.date, date, 20);
	strncpy(message.nick, nick,20);
	return msgsnd(queueId, &message,sizeof(message) - sizeof(long), IPC_NOWAIT);
}

int sendGroupMessageConfirmation(long pid, int status)
{
	msgbuf message;
	message.type = pid;
	message.cmd = 9;
	message.status = status;
	message.tab[0]='\0';
	return msgsnd(queueId, &message,sizeof(message) - sizeof(long), IPC_NOWAIT);
}

int sendGroupMessage( long pid, char date[20], char tab[1000], char nick[20])
{
	msgbuf message;
	message.type = pid;
	message.cmd = 9;
	strncpy(message.tab,tab,1000);
	strncpy(message.date, date, 20);
	strncpy(message.nick, nick,20);
	return msgsnd(queueId, &message,sizeof(message) - sizeof(long), IPC_NOWAIT);
}

int sendLogOutConfirmation( long pid, int status)
{
	msgbuf message;
	message.type = pid;
	message.cmd = 10;
	message.status = status;
	return msgsnd(queueId, &message,sizeof(message) - sizeof(long), IPC_NOWAIT);
}

msgbuf getRequest()
{
	msgbuf message;
	if(msgrcv(queueId, &message, sizeof(msgbuf)-sizeof(long), 1, 0)==-1)
	{
		message.cmd = -1;
	}
	return message;
}

