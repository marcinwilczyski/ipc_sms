#ifndef NETWORK_H
#define NETWORK_H

void establishConnection(int key);
int registerUser(char nick[20]);
int getUserList();
int getGroupList();
int changeUserName(char newNick[20]);
int joinToGroup(char groupName[20]);
int leaveGroup();
int sendMessage(char nick[20], char tab[1000]);
int sendGroupMessage(char tab[1000]);
int logout();
int refresh(char nick[20], char tab[1000], char date[20], int * status, int * cmd);
	

#endif