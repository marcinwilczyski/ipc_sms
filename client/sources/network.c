#include "../headers/network.h"

#include<time.h>
#include<sys/types.h>
#include<sys/ipc.h>
#include<sys/msg.h>
#include<string.h>
#include<unistd.h>

int queueId;
int pid;

typedef struct msgbuf msgbuf;

struct msgbuf 
{
	long type;
	int cmd;
	int pid;
	char nick[20];
	int status;
	char tab[1000];
	char date[20];
};

void getTime(char date[20])
{
	strncpy(date,"",20);
	unsigned t = time(0);
	unsigned h,m,s;
	s = t%60;
	t /=60;
	m = t%60;
	t/=60;
	h = t%24;
	char a;
	a = (char) ('0'+h/10);
	strncat(date,&a,1);
	a = (char) ('0'+h%10);
	strncat(date,&a,1);
	a = ':';
	strncat(date,&a,1);
	a = (char) ('0'+m/10);
	strncat(date,&a,1);
	a = (char) ('0'+m%10);
	strncat(date,&a,1);
	a = ':';
	strncat(date,&a,1);
	a = (char) ('0'+s/10);
	strncat(date,&a,1);
	a =(char) ('0'+s%10);
	strncat(date,&a,1);	
}

void establishConnection(int key)
{
	queueId = msgget(key, IPC_CREAT | 0644);
	pid = getpid();
}

int registerUser(char nick[20])
{
	msgbuf message;
	message.type = 1;
	message.cmd = 1;
	strncpy(message.nick,nick,20);
	message.pid = pid;
	return msgsnd(queueId, &message,sizeof(message) - sizeof(long), 0);
}

int getUserList()
{
	msgbuf message;
	message.type = 1;
	message.cmd = 2;
	message.pid = pid;
	return msgsnd(queueId, &message,sizeof(message) - sizeof(long), 0);
}

int getGroupList()
{
	msgbuf message;
	message.type = 1;
	message.cmd = 3;
	message.pid = pid;
	return msgsnd(queueId, &message,sizeof(message) - sizeof(long), 0);
}


int changeUserName(char newNick[20])
{
	msgbuf message;
	message.type = 1;
	message.cmd = 4;
	message.pid = pid;
	strncpy(message.nick, newNick,20);
	return msgsnd(queueId, &message,sizeof(message) - sizeof(long), 0);
}

int joinToGroup(char groupName[20])
{
	msgbuf message;
	message.type = 1;
	message.cmd = 5;
	message.pid = pid;
	strncpy(message.nick, groupName,20);
	return msgsnd(queueId, &message,sizeof(message) - sizeof(long), 0);
}

int leaveGroup()
{
	msgbuf message;
	message.type = 1;
	message.cmd = 6;
	message.pid = pid;
	msgsnd(queueId, &message,sizeof(message) - sizeof(long), 0);
	return message.status;
}

int sendMessage(char nick[20], char tab[1000])
{
	msgbuf message;
	message.type = 1;
	message.cmd = 8;
	message.pid = pid;
	strncpy(message.nick, nick,20);
	strncpy(message.tab, tab,1000);
	char date[20] = "";
	getTime(date);
	strncpy(message.date, date,20);
	return msgsnd(queueId, &message,sizeof(message) - sizeof(long), 0);
}

int sendGroupMessage(char tab[1000])
{
	msgbuf message;
	message.type = 1;
	message.cmd = 9;
	message.pid = pid;
	strncpy(message.tab, tab,1000);
	char date[20] = "";
	getTime(date);
	strncpy(message.date, date,20);
	return msgsnd(queueId, &message,sizeof(message) - sizeof(long), 0);
}

int logout()
{
	msgbuf message;
	message.type = 1;
	message.cmd = 10;
	message.pid = pid;
	return msgsnd(queueId, &message,sizeof(message) - sizeof(long), 0);
}

int refresh(char nick[20], char tab[1000], char date[20], int * status, int * cmd)
{
	msgbuf message;
	int value = msgrcv(queueId, &message, sizeof(msgbuf)-sizeof(long), pid, IPC_NOWAIT);
	if(value!=-1)
	{
		strncpy(date, message.date,20);
		strncpy(nick, message.nick,20);
		strncpy(tab, message.tab,1000);
		*status = message.status;
		*cmd = message.cmd;
	}
	return value;
}


