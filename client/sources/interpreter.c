#include "../headers/interpreter.h"

#include <string.h>
#include <stdio.h>

char TYPED_COMMANDS[11][20] = {
	":login", //+nick
	":ulist",
	":glist",
	":rename", //+nick
	":sign", //+nick
	":unsign", 
	":s",  //+nick
	":sg",
	":logout",
	":refresh",
	":help"
};

int translateCommand(char command[20])
{
	int nr = -1;
	int i;
	for(i=0;i<11;i++)
	{
		if(strncmp(command, TYPED_COMMANDS[i],20) == 0)
		{
			nr = i;
			break;
		}
	}
	return nr;
}

void removeSigns(char * str, int n)
{
	int len = strlen(str);
	if (n > len)
		return;
	memmove(str, str+n, len - n + 1);
}

int getWord(char * tab, char word[20])
{
	int toReturn = 0;
	while(tab[0] == ' ') removeSigns(tab,1);
	if(tab[0] != ':' ) toReturn = -1;
	int i=0;
	while(tab[i]!=' ' && i < 20 && tab[i]!='\0')
	{
		word[i] = tab[i];
		i++;
	}
	if(i<20) word[i] = '\0';
	while(tab[i] == ' ') i++;
	removeSigns(tab,i);	
	return toReturn;
}

int getCommand(char * tab)
{
	char word[20]="";
	if(getWord(tab,word) == 0)
	{
		return translateCommand(word);
	}
	else return -1;	
}

int interpret(char tab[1000], char nick[20])
{
	int command = getCommand(tab);
	if( command == 0 || command == 3 || command == 4 || command == 6)
	{
		getWord(tab, nick);
	}	
	return command;
}

void printHelp()
{
	int i;
	for(i=0;i<11;i++) printf("%s\n", TYPED_COMMANDS[i]);
}