#include "../headers/clientfunctions.h"

#include "../headers/network.h"
#include "../headers/interpreter.h"

#include<stdio.h>

char CMD_STATUS[10][50]={
	"",
	"OK!",
	"Nazwa zajeta!",
	"Jestes juz zalogowany!",
	"Nalezysz juz do grupy!",
	"Grupa nie istnieje!",
	"Nie nalezysz do zadnej grupy!",
	"Podany uzytkownik nie istnieje!",
	"Nie jestes zalogowany!",
	"Serwer przepelniony!"
};

char COMMANDS[11][50] ={
	"",
	"Rejestracja: ",
	"Lista uzytkownikow: ",
	"Lista grup: ",
	"Zmiana nazwy: ",
	"Zapisywanie do grupy: ",
	"Wypisywanie z grupy: ",
	"",
	"Prywatna wiadomosc: ",
	"Grupowa wiadomosc: ",
	"Wylogowanie: "
};

void displayMessage(char nick[20], char tab[1000], char date[20])
{
	printf("%s %s: %s\n", date, nick, tab);
}

void displayStatus(int cmd, int status)
{
	printf("%s%s\n", COMMANDS[cmd], CMD_STATUS[status]);
}

void displayList(int cmd, char tab[1000])
{
	printf("%s%s\n", COMMANDS[cmd], tab);
}

void display(char nick[20], char tab[1000], char date[20], int status, int cmd)
{
	if(cmd == 2 || cmd == 3)
	{
		displayList(cmd, tab);
	}
	else if(cmd == 1 || cmd == 4 || cmd == 5 || cmd == 6 ||
		 cmd == 10 || (cmd == 8 || cmd == 9) && tab[0] == '\0')
	{
		displayStatus(cmd, status);
	}
	else
	{
		displayMessage(nick, tab, date);
	}
}

void getMsg()
{
	char nick[20];
 	char tab[1000];
	char date[20];
	int status;
	int cmd;
	while(refresh(nick, tab, date, &status, &cmd) !=-1)
	{
		display(nick, tab, date, status, cmd);
	}
}

void displayError()
{
	printf("Wrong Command!\n");
}

void sendCommand(char tab[1000])
{
	char nick[20];
	int command = interpret(tab, nick);
	switch(command)
	{
		case 0: registerUser(nick);break;
		case 1: getUserList();break;
		case 2: getGroupList();break;
		case 3: changeUserName(nick);break;
		case 4: joinToGroup(nick);break;
		case 5: leaveGroup();break;
		case 6: sendMessage(nick, tab);break;
		case 7: sendGroupMessage(tab);break;
		case 8: logout();break;
		case 9: getMsg();break;
		case 10: printHelp();break;
		default: displayError();
	}
}

void initClient(int key)
{
	establishConnection(key);
}
